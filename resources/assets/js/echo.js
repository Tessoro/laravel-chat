import Event from './event';

export default class RoomConnection {
    connectToPresence(chatRoom) {
        window.Echo.join(chatRoom)
            .here((users) => {
                Event.$emit('users.here', users);
            })
            .leaving((user) => {
                Event.$emit('users.leaving', user);
            })
            .joining((user) => {
                Event.$emit('users.joined', user);
            })
            .listen('MessageCreated', (data) => {
                Event.$emit('added_message', data.message);
            })
            .listen('MessageDeleted', (data) => {
                Event.$emit('deleted_message', data.message);
            })
            .listen('MessageEdited', (data) => {
                Event.$emit('edited_message', data.message);
            });
    }

    connectToPrivate(chatRoom) {
        window.Echo.private(chatRoom)
            .listen('MessageCreated', (data) => {
                Event.$emit('added_message', data.message);
            });
    }

    connectToPublic(chatRoom) {
        window.Echo.auth(chatRoom)
            .listen('MessageCreated', (data) => {
                Event.$emit('added_message', data.message);
            });
    }
}