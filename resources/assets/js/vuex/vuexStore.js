import {FETCH_MESSAGES, ADD_MESSAGE, FETCH_ROOM_LIST, ADD_ROOM_TO_LIST, EDIT_ROOM, DELETE_ROOM, CHANGE_ROOM, DELETE_MESSAGE, EDIT_MESSAGE} from './mutation-types.js';

export default new Vuex.Store({
    state: {
        messages: [],
        rooms: [],
        room: null
    },
    mutations: {
        [FETCH_ROOM_LIST](state, rooms) {
            state.rooms = rooms;
        },
        [FETCH_MESSAGES](state, messages) {
            state.messages = messages;
        },
        [ADD_MESSAGE](state, message) {
            state.messages.unshift(message);
        },
        [ADD_ROOM_TO_LIST](state, room) {
            state.rooms.push(room);
        },
        [CHANGE_ROOM](state, room) {
            state.room = room;
            Event.$emit('room_changed', room);
        },
        [EDIT_ROOM](state, room) {
            state.room.name = room.name;
        },
        [DELETE_ROOM](state) {
            state.rooms.splice(state.rooms.indexOf(state.room), 1);
            state.room = null;
        },
        [DELETE_MESSAGE](state, index) {
            state.messages.splice(index, 1);
        },
        [EDIT_MESSAGE](state, message ) {
            let index = state.messages.indexOf(message);
            state.messages[index].body = message.body;
        }
    },
    getters: {
        getMessages: state => {
            return state.messages;
        },
        getRoom: state => {
            return state.room;
        },
        getRoomList: state => {
            return state.rooms;
        },
        getRoomName: state => {
            if(state.room !== null){
                return state.room.name;
            }
            return '';
        }
    },
    actions: {
        fetchMessages( {commit, state} ) {
            return new Promise( (ok) => {
                axios.get('/message/' + state.room.id).then((response) => {
                    commit(FETCH_MESSAGES, response.data);
                    ok();
                });
            });
        },
        fetchRooms( {commit} ) {
            return new Promise( (ok) => {
                axios.get('/room').then((response) => {
                    commit(FETCH_ROOM_LIST, response.data);
                    ok();
                })
            });
        },
        addMessage( {commit}, message ) {
            commit(ADD_MESSAGE, message);
        },
        editMessage( {commit, state}, message ){
            return new Promise( (ok) => {
                axios.post('/edit_message/', {
                    message: message,
                    method: 'edit_message'
                }).then( (response) => {
                    if(response.data){
                        commit(EDIT_MESSAGE, message);
                        ok();
                    }
                } );
            } );
        },
        addRoom( {commit}, response ) {
            commit(ADD_ROOM_TO_LIST, response.data.room);
        },
        setRoom( {commit}, room ) {
            return new Promise( (ok) => {
                commit(CHANGE_ROOM, room);
                ok();
            } );
        },
        editRoom( {commit, state}, room ) {
            return new Promise( (ok) => {
                axios.put('/room/' + room.id, {
                    room: room,
                    method: 'edit_room'
                }).then( (response) => {
                    commit(EDIT_ROOM, response.data);
                } );
                ok();
            })
        },
        deleteRoom( {commit, state} ){
            return new Promise( (ok) => {
                axios.delete('/room/' + state.room.id, {
                    room: state.room,
                    method: 'delete_room'
                }).then( (response) => {
                    if(response.data){
                        commit(DELETE_ROOM);
                    }
                    ok();
                } );
            } );
        },
        deleteMessage( {commit, state}, message ) {
            return new Promise( (ok) => {
                axios.post('/delete_message/', {
                    message: message,
                    method: 'delete_message'
                }).then( (response) => {
                    if(response.data){
                        ok(message);
                    }
                } );
            } );
        }
    }
});