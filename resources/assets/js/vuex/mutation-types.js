export const FETCH_MESSAGES = 'fetchMessages';
export const EDIT_ROOM = 'editRoom';
export const CHANGE_ROOM = 'changeRoom';
export const DELETE_ROOM = 'deleteRoom';
export const FETCH_ROOM_LIST = 'getRooms';
export const ADD_ROOM_TO_LIST = 'addRoom';
export const ADD_MESSAGE = 'addMessage';
export const EDIT_MESSAGE = 'editMessage';
export const DELETE_MESSAGE = 'deleteMessage';

export default {FETCH_MESSAGES, ADD_MESSAGE, FETCH_ROOM_LIST, ADD_ROOM_TO_LIST, CHANGE_ROOM, DELETE_MESSAGE, EDIT_MESSAGE};