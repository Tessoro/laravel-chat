require('./bootstrap');

import Event from './event';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';
import {Tabs, Tab} from 'vue-tabs-component';
window.Event = Event;
window.Vue = require('vue');
window.Vuex = Vuex;

Vue.use(VModal, { dialog: true });
Vue.use(window.Vuex);

require('./vuex/vuexStore');

Vue.component('tabs', Tabs);
Vue.component('tab', Tab);

Vue.component('room-list-component', require('./components/Room/RoomListComponent.vue'));
Vue.component('whole-chat-component', require('./components/WholeChatComponent.vue'));
Vue.component('chat-component', require('./components/ChatComponent.vue'));
Vue.component('user-component', require('./components/UserComponent.vue'));
Vue.component('chat-messages-component', require('./components/Message/ChatMessagesComponent.vue'));
Vue.component('chat-form-component', require('./components/ChatFormComponent.vue'));
Vue.component('message-component', require('./components/Message/MessageComponent.vue'));
Vue.component('create-room-component', require('./components/modals/CreateRoomComponent.vue'));
Vue.component('edit-room-component', require('./components/modals/EditRoomComponent.vue'));
Vue.component('edit-context-menu', require('./components/Context/EditContextMenu.vue'));
Vue.component('edit-message-menu', require('./components/Context/EditMessageMenu.vue'));

const app = new Vue({
    el: '#app'
});

require('./echo');
