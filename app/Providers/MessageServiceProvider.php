<?php

namespace App\Providers;

use App\Events\MessageCreated;
use App\Events\MessageDeleted;
use App\Events\MessageEdited;
use App\Message;
use App\Repositories\MessageRepository;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class MessageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ShouldBroadcast::class, function ($app) {
            $request = $app->make(Request::class);
            $messageRepository = $app->make(MessageRepository::class);
            if($request->method === 'create_message'){
                $message = $messageRepository->createMessage($request);
                return new MessageCreated($message);
            }
            elseif($request->method === 'edit_message'){
                $message = $messageRepository->editMessage($request);
                return new MessageEdited($message);
            }
            elseif($request->method === 'delete_message'){
                $message = $messageRepository->deleteMessage($request);
                return new MessageDeleted($message);
            }
        });
    }
}
