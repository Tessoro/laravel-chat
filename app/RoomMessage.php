<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomMessage extends Model
{
    protected $fillable = ['room_id', 'message_id'];

    protected $table = 'room_messages';

    public function getMessages() {
        return Message::where('id', $this->message_id);
    }
}
