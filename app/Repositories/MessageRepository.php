<?php


namespace App\Repositories;


use App\Message;
use App\Room;
use App\RoomMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MessageRepository
{
    public function createMessage(Request $request) {
        if(Room::find($request->input('room_id')) ){
            $user = $request->user();
            $message = $user->messages()->create([
                'body' => $request->body
            ]);

            $roomMessage = RoomMessage::create([
                'room_id' => $request->input('room_id'),
                'message_id' => $message->id
            ]);

            if($roomMessage){
                return $message;
            }
            else{
                return false;
            }
        }
    }

    public function deleteMessage(Request $request) {
        Message::find($request->message['id'])->delete();
        return $request->message;
    }

    public function editMessage(Request $request) {
        $message = Message::find($request->message['id'])->fill($request->message);
        $message->save();
        Log::info($message);

        return $message;
    }
}