<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    protected $fillable = ['body'];

    protected $appends = ['selfMessage'];

    protected $table = 'messages';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getSelfMessageAttribute() {
        if(Auth::user()){
            return $this->getAttribute('user_id') == Auth::user()->getAuthIdentifier();
        }
        return false;
    }
}
