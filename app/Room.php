<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable =[
        'name',
        'owner_id'
    ];

    protected $table = 'rooms';


    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }
}
