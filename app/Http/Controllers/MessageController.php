<?php

namespace App\Http\Controllers;

use App\Message;
use App\Room;
use App\RoomMessage;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::with(['user'])->orderByDesc('created_at')->get();

        return response()->json($messages);
    }

    public function store(Request $request, ShouldBroadcast $broadcast)
    {
        broadcast($broadcast)
            ->toOthers();
        return response()->json(true);
    }

    public function getMessagesByRoom($roomId) {
        $roomMessages = RoomMessage::where('room_id', $roomId)->orderByDesc('created_at')->get();
        $messages = [];
        foreach ($roomMessages as $roomMessage) {
            $messages[] = $roomMessage->getMessages()->with(['user'])->get()->first();
        }

        return response()->json($messages);
    }

    public function deleteMessage(Request $request, ShouldBroadcast $broadcast) {
        broadcast($broadcast);
        return response()->json(true);
    }

    public function editMessage(Request $request, ShouldBroadcast $broadcast) {
        broadcast($broadcast);
        return response()->json(true);
    }
}