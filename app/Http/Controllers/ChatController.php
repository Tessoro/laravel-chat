<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth', 'web']);
    }

    public function index()
    {
        return view('chat');
    }
}