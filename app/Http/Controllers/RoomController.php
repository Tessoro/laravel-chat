<?php

namespace App\Http\Controllers;

use App\Room;
use App\RoomUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            Auth::user()->rooms
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $roomName = $request->input('name');
        $userId = $request->user()->id;
        $room = null;

        if($roomName){
            DB::transaction(function () use (&$roomName, &$userId, &$room) {
                $room = Room::create([
                    'owner_id' => $userId,
                    'name' => $roomName,
                ]);

                if($room){
                    RoomUser::create([
                        'room_id' => $room->id,
                        'user_id' => $userId,
                    ]);
                };
            });
            return response()->json([
                'status' => true,
                'room' => $room
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => "Room name is empty :("
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        return response()->json($room);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room->fill($request->input('room'));

        $room->save();
        return response()->json($room);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Room $room)
    {
        return response()->json($room->delete());
    }
}
