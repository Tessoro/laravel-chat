<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::middleware(['auth', 'web'])->group( function() {
    Route::get('/message/{roomId}', 'MessageController@getMessagesByRoom')->name('message');
    Route::get('/chat', 'ChatController@index')->name('chat');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resources([
        'room' => 'RoomController'
    ]);
    Route::resources([
        'message' => 'MessageController'
    ]);
    Route::post('/delete_message', 'MessageController@deleteMessage')->name('delete_message');
    Route::post('/edit_message', 'MessageController@editMessage')->name('edit_message');
});